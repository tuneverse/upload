package driver

import (
	"event/internal/entities"
	"fmt"

	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"github.com/aws/aws-sdk-go/aws"
	"gitlab.com/tuneverse/toolkit/core/awsmanager"
	"gitlab.com/tuneverse/toolkit/core/queue"
)

type Queue interface {
	InitQueueConfig(cfg *entities.EnvConfig, awsConf *awsmanager.AwsConfig) (queue.Queue, error)
}

type RabbitMQQueue struct {
	Config *queue.RabbitMQConfig
}

func (r *RabbitMQQueue) InitQueueConfig(cfg *entities.EnvConfig, awsConf *awsmanager.AwsConfig) (queue.Queue, error) {
	rabbitMQQueue, err := queue.NewRabbitMQQueue(r.Config)
	return rabbitMQQueue, err
}

type SQSQueue struct {
	Config *queue.SQSConfig
}

func (s *SQSQueue) InitQueueConfig(cfg *entities.EnvConfig, awsConf *awsmanager.AwsConfig) (queue.Queue, error) {
	sqsQueue, err := queue.NewSQSQueue(awsConf, s.Config)
	return sqsQueue, err
}

func InitQueue(cfg *entities.EnvConfig, awsConf *awsmanager.AwsConfig) (queue.Queue, error) {
	var messageQueue Queue
	mqType := cfg.MessageQueueType
	switch mqType {
	case "rabbitmq":
		messageQueue = &RabbitMQQueue{
			Config: &queue.RabbitMQConfig{
				URL:        prapareRabbitMQConnString(&cfg.Rabbitmq),
				Name:       cfg.Rabbitmq.QueueName,
				Durable:    true,
				AutoDelete: false,
				Exclusive:  false,
				NoWait:     false,
				Internal:   false,
				Arguments:  nil,
				Mandatory:  false,
				Immediate:  false,
				NoLocal:    false,
				AutoAck:    false,
				Consumer:   "",
				Args:       nil,
			},
		}
	case "sqs":
		messageQueue = &SQSQueue{
			Config: &queue.SQSConfig{
				QueueInfo: &sqs.CreateQueueInput{
					QueueName: aws.String(cfg.AWS.SqsQueueName),
				},
				ReceiveMessageConfig: &sqs.ReceiveMessageInput{
					MaxNumberOfMessages: 5,
					VisibilityTimeout:   30,
					WaitTimeSeconds:     20,
				},
			},
		}
	default:
		return nil, fmt.Errorf("unsupported message queue type: %s", mqType)
	}

	queue, err := messageQueue.InitQueueConfig(cfg, awsConf)
	if err != nil {
		return nil, fmt.Errorf("error initializing message queue: %v", err)
	}
	return queue, nil
}

func prapareRabbitMQConnString(cfg *entities.Rabbitmq) string {
	conn := fmt.Sprintf("amqp://%s:%s@%s:%s/", cfg.User, cfg.Password, cfg.Host, cfg.Port)
	return conn
}
