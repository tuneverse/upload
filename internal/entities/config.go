package entities

// EnvConfig represents the configuration structure for the application.
type EnvConfig struct {
	Debug            bool     `default:"true" split_words:"true"` // Indicates whether the application is in debug mode (default: true)
	LoggerServiceURL string   `split_words:"true"`                // URL for the logger service
	LoggerSecret     string   `split_words:"true"`                // Secret key for logging
	StorageType      string   `split_words:"true"`
	AWS              AWS      `split_words:"true"` //  Aws configuration
	Rabbitmq         Rabbitmq `split_words:"true"`
	TrackApiUrl      string   `split_words:"true"`
	MessageQueueType string   `split_words:"true"`
}

// AWS represents AWS configuration settings, including access key, access secret, and region.
type AWS struct {
	AccessKey    string `split_words:"true"`
	AccessSecret string `split_words:"true"`
	Region       string
	BucketName   string `split_words:"true"`
	SqsQueueName string `split_words:"true"`
	SqsQueueUrl  string `split_words:"true"`
}

type Rabbitmq struct {
	Host      string
	User      string
	Password  string
	Port      string
	QueueName string
}

type QueueMessage struct {
	EventID       string
	TrackID       string
	MemberID      string
	PartnerID     string
	FileExtension string
}

type MessageBody struct {
	TaskType string
	Payload  map[string]interface{}
}

type Source struct {
	Storage      string `json:"storage"`
	FilePath     string `json:"filePath"`
	Bucket       string `json:"bucket"`
	BucketRegion string `json:"region"`
}
