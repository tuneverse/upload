package consts

// Constants defining fundamental properties and settings of the application.
const (
	AppName = "event"
)

// logger settings
const (
	LogMaxAge    = 1
	LogMaxSize   = 1024 * 1024 * 10
	LogMaxBackup = 5
)

const (
	TempFilePath  = "tuneverse/temp/tracks/"
	FileExtension = "flac"
)

// s3 expiration
const (
	Expiration = 2

	//Maximum Retry Attemps if the Conversion fails
	MaxAttempts = 3

	//delay seconds before reattempting a retry
	DelaySeconds = 5
)
