FROM golang:1.21.3-alpine3.18 AS build-stage

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o /event


# Deploy the application binary into a lean image
FROM alpine:3.18

WORKDIR /

COPY --from=build-stage /event /event

EXPOSE 8080

ENTRYPOINT ["/event"]

