package app

import (
	"context"
	serviceconfig "event/config"
	"event/consumer"
	"event/internal/consts"
	"event/internal/driver"
	"event/internal/entities"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/gin-gonic/gin"
	"gitlab.com/tuneverse/toolkit/core/awsmanager"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/middleware"
	"gitlab.com/tuneverse/toolkit/utils"
)

// Run initializes environment configuration, logging, database connection, and API routing.
// It sets up the necessary components and routes for the application and launches it.
func Run() {
	// init the env config
	cfg, err := serviceconfig.LoadConfig(consts.AppName)
	if err != nil {
		panic(err)
	}

	// Create a file logger configuration with specified settings.
	file := &logger.FileMode{
		LogfileName:  "event.log",
		LogPath:      "logs",
		LogMaxAge:    consts.LogMaxAge,
		LogMaxSize:   consts.LogMaxSize,
		LogMaxBackup: consts.LogMaxBackup,
	}
	// Configuring client options for the logger.
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName,
		LogLevel:            "info",
		IncludeRequestDump:  false,
		IncludeResponseDump: false,
		JSONFormater:        false,
	}
	if cfg.Debug {
		logger.InitLogger(clientOpt, file)
	} else {
		// Create a database logger configuration with the specified URL and secret.
		db := &logger.CloudMode{
			URL:    cfg.LoggerServiceURL,
			Secret: cfg.LoggerSecret,
		}
		// Initialize the logger with the specified configurations for database, file, and console logging.
		logger.InitLogger(clientOpt, db, file)
	}

	router := gin.New()
	// middleware initialization
	router.Use(middleware.LogMiddleware(map[string]interface{}{}))

	//initialises aws
	awsConfig, err := awsInit(cfg)
	if err != nil {
		log.Fatalf("Failed to initialize aws, err=%s", err.Error())
		return
	}

	queue, nil := driver.InitQueue(cfg, awsConfig)
	if err != nil {
		log.Fatalf("unable to connect the queue, err=%s", err.Error())
		return
	}

	//creating a job consumer
	consumer := consumer.NewConsumer(queue, cfg, awsConfig)

	//initialises task scheduler
	initConsumer(consumer)
}

func awsInit(cfg *entities.EnvConfig) (*awsmanager.AwsConfig, error) {
	var optFns []func(*config.LoadOptions) error
	if !utils.IsEmpty(cfg.AWS.AccessKey) && !utils.IsEmpty(cfg.AWS.AccessSecret) {
		optFns = append(optFns, awsmanager.WithCredentialsProvider(cfg.AWS.AccessKey, cfg.AWS.AccessSecret))
	}
	if !utils.IsEmpty(cfg.AWS.Region) {
		optFns = append(optFns, awsmanager.WithRegion(cfg.AWS.Region))
	}
	awsConf, err := awsmanager.CreateAwsSession(optFns...)
	if err != nil {
		return nil, err
	}
	return awsConf, nil
}

// initConsumer initialises a consumer
func initConsumer(cons consumer.ConsumerImply) {

	//initialising cron jobs
	log.Println("Server started....")

	logger.Log().Info("server started")
	addJobs(cons)

	quit := make(chan os.Signal, 1)
	// kill (no param) default send syscanll.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall. SIGKILL but can"t be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Printf("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	// catching ctx.Done(). timeout of 2 seconds.
	<-ctx.Done()
	log.Printf("timeout of 2 seconds.")

	log.Printf("Server exiting")
}

// addCronJobs adds a cron job to the scheduler
func addJobs(sch consumer.ConsumerImply) {
	ctx := context.Background()
	sch.ProcessMessages(ctx)
}
