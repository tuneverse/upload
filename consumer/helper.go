package consumer

import (
	"bytes"
	"context"
	"event/internal/consts"
	"event/internal/entities"
	"fmt"
	"io"
	"log"
	"time"

	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/go-flac/go-flac"
)

func (cu *Consumer) BuildSourceURL(ctx context.Context, src entities.Source) (string, error) {
	if src.Storage == "s3" {
		req, err := s3.NewPresignClient(cu.AwsConf.S3()).PresignGetObject(ctx, &s3.GetObjectInput{
			Bucket: aws.String(src.Bucket),
			Key:    aws.String(src.FilePath),
		}, func(opts *s3.PresignOptions) {
			opts.Expires = time.Duration(consts.Expiration * int(time.Hour))
		})

		// Generate pre-signed URL
		if err != nil {
			return "", fmt.Errorf("unable to generate pre-signed URL: %w", err)
		}
		return req.URL, nil
	} else {
		return src.FilePath, nil
	}
}

func (cu *Consumer) FileExistsInS3(ctx context.Context, bucketName, objectKey string) (bool, error) {
	_, err := cu.AwsConf.S3().HeadObject(ctx, &s3.HeadObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(objectKey),
	})
	if err != nil {
		log.Printf("Error checking file existence in S3: %v", err)
		return false, err
	}
	// If the object exists, return true
	return true, nil
}

func (cu *Consumer) UploadDataToS3(ctx context.Context, bucketName, objectKey string, data []byte) error {
	_, err := cu.AwsConf.S3().PutObject(ctx, &s3.PutObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(objectKey),
		Body:   bytes.NewReader(data),
	})
	return err
}

func (cu *Consumer) DeleteFileFromS3(ctx context.Context, bucketName, objectKey string) error {
	// Delete the file from the S3 temp path
	_, err := cu.AwsConf.S3().DeleteObject(ctx, &s3.DeleteObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(objectKey),
	})
	return err
}

func (cu *Consumer) GetFileSizeFromS3(ctx context.Context, bucketName, objectKey string) (int64, error) {
	headObjectOutput, err := cu.AwsConf.S3().HeadObject(ctx, &s3.HeadObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(objectKey),
	})
	if err != nil {
		log.Printf("Error getting file size from S3: %v", err)
		return 0, err
	}
	return headObjectOutput.ContentLength, nil
}

func (cu *Consumer) ReadFileFromS3(ctx context.Context, bucketName, objectKey string) ([]byte, error) {

	exists, err := cu.FileExistsInS3(ctx, bucketName, objectKey)
	if err != nil {
		log.Printf("Error checking file existence in S3: %v", err)
		return nil, err
	}
	if !exists {
		// Handle the case where the object does not exist
		log.Printf("File not found in S3: %s/%s", bucketName, objectKey)
		return nil, fmt.Errorf("file not found in S3")
	}
	output, err := cu.AwsConf.S3().GetObject(ctx, &s3.GetObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(objectKey),
	})
	if err != nil {
		return nil, err
	}
	defer output.Body.Close()

	data, err := io.ReadAll(output.Body)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (cu *Consumer) GetFlacSampleRateFromS3(ctx context.Context, bucketName, objectKey string) (float64, error) {
	s3object, err := cu.AwsConf.S3().GetObject(ctx, &s3.GetObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(objectKey),
	})
	if err != nil {
		log.Printf("Error checking file existence in S3: %v", err)
	}
	defer s3object.Body.Close()

	file, err := flac.ParseBytes(s3object.Body)
	if err != nil {
		return 0, err
	}

	// Get the stream info
	streamInfo, err := file.GetStreamInfo()
	if err != nil {
		return 0, err
	}

	sampleRate := float64(streamInfo.SampleRate)
	return sampleRate, nil
}
