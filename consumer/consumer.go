package consumer

import (
	"bytes"
	"context"
	"encoding/json"
	"event/internal/consts"
	"event/internal/entities"
	"fmt"
	"log"
	"net/http"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"github.com/rabbitmq/amqp091-go"
	ffmpeg "github.com/u2takey/ffmpeg-go"
	"gitlab.com/tuneverse/toolkit/core/awsmanager"
	"gitlab.com/tuneverse/toolkit/core/queue"
	"gitlab.com/tuneverse/toolkit/utils"
)

type Consumer struct {
	Config        *entities.EnvConfig
	Queue         queue.Queue
	AwsConf       *awsmanager.AwsConfig
	FfmpegOptions ffmpeg.KwArgs
}

type ConsumerImply interface {
	ProcessMessages(ctx context.Context)
}

func NewConsumer(queue queue.Queue, config *entities.EnvConfig, awsConfig *awsmanager.AwsConfig) ConsumerImply {
	return &Consumer{
		Config:  config,
		Queue:   queue,
		AwsConf: awsConfig,
	}
}

func (cu *Consumer) ProcessMessages(ctx context.Context) {
	// Receive messages
	deliveries, err := cu.Queue.Receive(ctx)
	if err != nil {
		log.Fatalf("Error consuming messages: %v", err)
	}
	// Use a switch case to handle different types of deliveries
	switch v := deliveries.(type) {
	case <-chan amqp091.Delivery:
		cu.ProcessRabbitMQMessages(ctx, v)
	case *sqs.ReceiveMessageOutput:
		cu.ProcessSQSMessages(ctx, v)
	default:
		log.Fatalf("Error: Unsupported type of deliveries")
	}
}

func (cu *Consumer) ProcessSQSMessages(ctx context.Context, receiveMessageOutput *sqs.ReceiveMessageOutput) {
	if receiveMessageOutput == nil || receiveMessageOutput.Messages == nil {
		log.Println("Info: Nil or empty SQS messages. Skipping.")
		cu.ProcessMessages(ctx)
	}

	for _, msg := range receiveMessageOutput.Messages {
		defer func() {
			if r := recover(); r != nil {
				log.Printf("Recovered from panic: %v", r)
			}
		}()

		log.Printf("Raw Message Body: %q", aws.ToString(msg.Body))

		if !json.Valid([]byte(aws.ToString(msg.Body))) {
			log.Printf("Error: Invalid JSON format in message body. Skipping message.")
			continue
		}

		cu.ProcessTask(ctx, []byte(aws.ToString(msg.Body)), aws.ToString(msg.ReceiptHandle))
	}
}

func (cu *Consumer) ProcessRabbitMQMessages(ctx context.Context, deliveryChan <-chan amqp091.Delivery) {
	// Process messages
	for delivery := range deliveryChan {

		go func(data amqp091.Delivery) {

			defer func() {
				if r := recover(); r != nil {
					log.Printf("Recovered from panic: %v", r)
				}
			}()

			deliveryTag := data.DeliveryTag
			messageBytes := data.Body

			// Log the raw message body
			log.Printf("Raw Message Body: %q", messageBytes)

			// Check if the message body is a valid JSON string
			if !json.Valid(messageBytes) {
				log.Printf("Error: Invalid JSON format in message body. Skipping message.")
				return
			}
			// Pass the correct context to ProcessTask
			cu.ProcessTask(ctx, messageBytes, deliveryTag)
		}(delivery)
	}
}

func (cu *Consumer) ProcessTask(ctx context.Context, messageBytes []byte, deliveryTag interface{}) {

	var body entities.MessageBody
	err := json.Unmarshal(messageBytes, &body)
	if err != nil {
		log.Printf("Error decoding custom message: %v", err)
	}
	payload, ok := body.Payload["inputPayload"]
	if !ok {
		log.Println("Error: Payload not found")
	}

	payloadJSON, err := json.Marshal(payload)
	if err != nil {
		log.Printf("Error encoding payload to JSON: %v", err)
	}

	switch body.TaskType {
	case "convertion":

		var msg entities.QueueMessage
		err = json.Unmarshal(payloadJSON, &msg)
		if err != nil {
			log.Printf("Error decoding custom message: %v", err)
		}
		// Process conversion messages
		trackID := msg.TrackID
		userID := msg.MemberID
		partnerID := msg.PartnerID
		fileExtension := msg.FileExtension
		var deliveryTagStr string
		switch v := deliveryTag.(type) {
		case string:
			deliveryTagStr = v
		case uint64:
			deliveryTagStr = strconv.FormatUint(v, 10)
		default:
			log.Printf("Error: Unsupported type for deliveryTag - %T", v)
			return
		}
		cu.ProcessAndDeleteMessage(ctx, deliveryTagStr, trackID, userID, partnerID, fileExtension)
	default:
		log.Printf("Skipping message with unexpected TaskType: %s", body.TaskType)
	}
}

func (cu *Consumer) ProcessAndDeleteMessage(ctx context.Context, messageID, trackID, userID, partnerID, fileExtension string) {

	tempPath := consts.TempFilePath
	actualPath := fmt.Sprintf("tuneverse/%s/%s/tracks/%s.flac", partnerID, userID, trackID)
	bucketName := cu.Config.AWS.BucketName
	tempFilePath := tempPath + trackID
	actualFilePath := actualPath

	switch cu.Config.StorageType {
	case "s3":
		// Handle conversion and saving to S3
		if err := cu.ConvertAndSaveToS3(ctx, bucketName, tempFilePath, actualFilePath, fileExtension); err != nil {
			log.Printf("Failed to convert and save S3 object: %v", err)
			// Update status on failure
			apiUrl := fmt.Sprintf("%s/%s/update-status", cu.Config.TrackApiUrl, trackID)
			fileSize, err := cu.GetFileSizeFromS3(ctx, bucketName, actualFilePath)
			if err != nil {
				return
			}
			sampleRate, err := cu.GetFlacSampleRateFromS3(ctx, bucketName, actualFilePath)
			if err != nil {
				return
			}
			// Create a payload for the API request
			payload := map[string]interface{}{
				"track_id":       trackID,
				"member_id":      userID,
				"partner_id":     partnerID,
				"status":         5,
				"file_size":      fileSize,
				"file_extension": "flac",
				"samplerate":     sampleRate,
			}
			// Make the APIRequest
			headers := make(map[string]interface{})
			headers["Content-Type"] = "application/json"

			response, err := utils.APIRequest("PATCH", apiUrl, headers, payload)
			if err != nil {
				log.Printf("failed to update status %v", err)
				return
			}
			if response.StatusCode != http.StatusOK {
				log.Println("Failed to update the track data after conversion")
				return
			}
		}
		// Handle file deletion
		if err := cu.DeleteFile(ctx, bucketName, tempFilePath); err != nil {
			log.Printf("Failed to delete file from S3 : %v", err)
			return
		}
	default:
		log.Printf("Unknown storage type: %s", cu.Config.StorageType)
	}

	// Handle message deletion
	if err := cu.DeleteMessage(ctx, messageID); err != nil {
		log.Printf("Failed to delete messages from queue : %v", err)
		return
	}

	apiUrl := fmt.Sprintf("%s/%s/update-status", cu.Config.TrackApiUrl, trackID)
	fileSize, err := cu.GetFileSizeFromS3(ctx, bucketName, actualFilePath)
	if err != nil {
		log.Printf("Failed to get filesize from s3 : %v", err)
		return
	}

	sampleRate, err := cu.GetFlacSampleRateFromS3(ctx, bucketName, actualFilePath)
	if err != nil {
		return
	}
	// Create a payload for the API request
	payload := map[string]interface{}{
		"track_id":       trackID,
		"member_id":      userID,
		"partner_id":     partnerID,
		"status":         4,
		"file_size":      fileSize,
		"file_extension": consts.FileExtension,
		"samplerate":     sampleRate,
	}

	// Make the APIRequest
	headers := make(map[string]interface{})
	headers["Content-Type"] = "application/json"

	response, err := utils.APIRequest("PATCH", apiUrl, headers, payload)
	if err != nil {
		log.Printf("Failed to update convertion status : %v", err)
		return
	}
	defer response.Body.Close()
	cu.ProcessMessages(ctx)

}

func (cu *Consumer) ConvertAndSaveToS3(ctx context.Context, bucketName, tempFilePath, actualFilePath, fileExtension string) error {

	var convertedData []byte

	inputData, err := cu.ReadFileFromS3(ctx, bucketName, tempFilePath)
	if err != nil {
		log.Printf("Failed to download S3 object: %v", err)
		return err
	}

	err = WithRetry(ctx, consts.MaxAttempts, consts.DelaySeconds, func() error {
		convertedData, err = cu.ConvertToFLAC(inputData, fileExtension, tempFilePath)
		return err
	})

	if err != nil {
		log.Printf("FFmpeg conversion failed for %s: %v", tempFilePath, err)
		return err
	}

	// Save the converted file to the destination path in S3
	err = cu.UploadDataToS3(ctx, bucketName, actualFilePath, convertedData)
	if err != nil {
		log.Printf("Failed to upload FLAC file to S3: %v", err)
		return err
	}
	fmt.Printf("Converted and saved %s \n", actualFilePath)

	return nil
}

func (cu *Consumer) DeleteFile(ctx context.Context, bucketName, objectKey string) error {
	// Delete the file from the temp path
	err := cu.DeleteFileFromS3(ctx, bucketName, objectKey)
	if err != nil {
		log.Printf("Failed to delete file from temp path: %v", err)
		return err
	}

	return nil
}

func (cu *Consumer) DeleteMessage(ctx context.Context, messageID string) error {
	// Delete the message from the queue
	err := cu.Queue.Delete(ctx, messageID)
	if err != nil {
		log.Printf("Error deleting message with ID %s: %v", messageID, err)

		return err
	}
	log.Printf("Successfully deleted message with ID: %s", messageID)

	return nil
}

func (cu *Consumer) ConvertToFLAC(inputData []byte, fileExtension, tempFilePath string) ([]byte, error) {
	ctx := context.Background()
	cu.FfmpegOptions = make(ffmpeg.KwArgs)
	cu.FfmpegOptions["format"] = strings.Replace(fileExtension, ".", "", -1)
	source := entities.Source{
		Storage:      "s3", // Assuming it's stored in S3
		FilePath:     tempFilePath,
		Bucket:       cu.Config.AWS.BucketName,
		BucketRegion: cu.Config.AWS.Region,
	}
	sourceURL, err := cu.BuildSourceURL(ctx, source)
	if err != nil {
		return nil, err
	}
	// Use ffmpeg.KwArgs to build the conversion command
	ffmpegCmd := exec.Command("ffmpeg", "-i", sourceURL, "-f", "flac", "-y", "-")
	ffmpegCmd.Stderr = &bytes.Buffer{}
	// Capture the standard output
	var convertedData bytes.Buffer
	ffmpegCmd.Stdout = &convertedData

	// Run the command
	err = ffmpegCmd.Run()
	if err != nil {
		log.Printf("FFmpeg error: %v", err)
		log.Printf("FFmpeg output: %s", ffmpegCmd.Stderr.(*bytes.Buffer).String())
		return nil, err
	}

	// Return the converted data
	return convertedData.Bytes(), nil
}

// RetryableFunc is a function signature for functions that can be retried.
type RetryableFunc func() error

// WithRetry executes a retryable function with a specified maximum number of attempts and delay between attempts.
// It returns the error encountered during the final attempt or nil if the function succeeds within the given attempts.
//
// Parameters:
//   - ctx: The context.Context for the operation.
//   - maxAttempts: The maximum number of attempts to execute the retryable function.
//   - delaySeconds: The duration to wait between retry attempts, in seconds.
//   - retryableFunc: The function that can be retried. It should return an error.
//
// Returns:
//   - An error representing the outcome of the final attempt or nil if successful.
func WithRetry(ctx context.Context, maxAttempts, delaySeconds int, retryableFunc RetryableFunc) error {
	var err error

	for attempt := 1; attempt <= maxAttempts; attempt++ {
		err = retryableFunc()
		if err == nil {
			// Success, break out of the loop
			break
		}

		log.Printf("Attempt %d failed: %v", attempt, err)

		// Wait for some duration before retrying (you can adjust this based on your needs)
		time.Sleep(time.Second * time.Duration(delaySeconds))
	}

	return err
}
